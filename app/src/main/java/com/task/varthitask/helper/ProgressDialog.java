package com.task.varthitask.helper;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.widget.TextView;

import com.task.varthitask.R;

public class ProgressDialog {
    static Dialog dialog;

    public static void showProgressDialog(Context mContext, String strMessage) {
        if (dialog == null) {
            dialog = new Dialog(mContext);
            dialog.setContentView(R.layout.progress_alert_dialog);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            HiveProgressView progressView = dialog.findViewById(R.id.hive_progress);

            TextView message = dialog.findViewById(R.id.message);
            message.setText(strMessage);
            if (!dialog.isShowing()) {
                dialog.show();
            }
        }

    }

    public static void hideProgressDialog() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
                dialog.cancel();
                dialog = null;
            }
        }
    }
}
