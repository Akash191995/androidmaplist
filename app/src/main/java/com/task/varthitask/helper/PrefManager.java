package com.task.varthitask.helper;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {
    // Shared preferences file name
    private static final String PREF_NAME = "Janahita";
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    // shared pref mode
    int PRIVATE_MODE = 0;
    private static final String MARKERNAME = "markername";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void markername(String markername){
        editor.putString(MARKERNAME, markername);
        editor.commit();
    }

    public String getMarkername() {
        return pref.getString(MARKERNAME, "");
    }


}
