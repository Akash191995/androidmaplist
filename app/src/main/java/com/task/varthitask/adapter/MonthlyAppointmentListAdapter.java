package com.task.varthitask.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.task.varthitask.R;
import com.task.varthitask.model.Unassigned;
import java.util.List;

public class MonthlyAppointmentListAdapter extends RecyclerView.Adapter<MonthlyAppointmentListAdapter.Viewholder> {
    Context context;
    List<Unassigned> unassignedsList;
    MonthlyAppointmentListAdapter.MonthlyAppointmentInterface monthlyAppointmentInterface;

    public interface MonthlyAppointmentInterface{
        void  onClickMapIcon(String locationName,String pincode);
    }

    public MonthlyAppointmentListAdapter(Context context, List<Unassigned> unassignedsList,MonthlyAppointmentListAdapter.MonthlyAppointmentInterface monthlyAppointmentInterface) {
        this.context = context;
        this.unassignedsList=unassignedsList;
        this.monthlyAppointmentInterface=monthlyAppointmentInterface;
    }

    @NonNull
    @Override
    public MonthlyAppointmentListAdapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_view, parent, false);
        return new MonthlyAppointmentListAdapter.Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MonthlyAppointmentListAdapter.Viewholder holder, int position) {

        holder.tvInstName.setText(unassignedsList.get(position).getInstitutionName());
        holder.tvPocName.setText(unassignedsList.get(position).getpOCName());
        holder.tvEmpId.setText(unassignedsList.get(position).getMarketingUserEmployeeId());

        holder.ivMapIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                monthlyAppointmentInterface.onClickMapIcon(unassignedsList.get(position).getAddressLine1()+"  "+unassignedsList.get(position).getAddressLine2(), String.valueOf(unassignedsList.get(position).getpIN()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return unassignedsList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private TextView tvInstName,tvPocName,tvEmpId;
        ImageView ivMapIcon;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            tvInstName=itemView.findViewById(R.id.tv_inst_name);
            tvPocName=itemView.findViewById(R.id.tv_poc_name);
            tvEmpId=itemView.findViewById(R.id.tv_emp_id);
            ivMapIcon=itemView.findViewById(R.id.iv_map_icon);
        }
    }
}
