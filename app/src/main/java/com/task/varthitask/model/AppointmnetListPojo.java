package com.task.varthitask.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppointmnetListPojo {
    @SerializedName("unassigned")
    @Expose
    private List<Unassigned> unassigned = null;
    @SerializedName("assigned")
    @Expose
    private List<Object> assigned = null;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("status_code")
    @Expose
    private Integer statusCode;

    public AppointmnetListPojo() {

    }

    public AppointmnetListPojo(List<Unassigned> unassigned, List<Object> assigned, String message, String status, Integer statusCode) {
        this.unassigned = unassigned;
        this.assigned = assigned;
        this.message = message;
        this.status = status;
        this.statusCode = statusCode;
    }

    public List<Unassigned> getUnassigned() {
        return unassigned;
    }

    public void setUnassigned(List<Unassigned> unassigned) {
        this.unassigned = unassigned;
    }

    public List<Object> getAssigned() {
        return assigned;
    }

    public void setAssigned(List<Object> assigned) {
        this.assigned = assigned;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public String toString() {
        return "AppointmnetListPojo{" +
                "unassigned=" + unassigned +
                ", assigned=" + assigned +
                ", message='" + message + '\'' +
                ", status='" + status + '\'' +
                ", statusCode=" + statusCode +
                '}';
    }
}
