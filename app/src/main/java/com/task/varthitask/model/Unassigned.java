package com.task.varthitask.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Unassigned {
    @SerializedName("Institution_Status")
    @Expose
    private String institutionStatus;
    @SerializedName("site")
    @Expose
    private String site;
    @SerializedName("PIN")
    @Expose
    private Integer pIN;
    @SerializedName("appointment_id")
    @Expose
    private String appointmentId;
    @SerializedName("custom_appointment_id")
    @Expose
    private String customAppointmentId;
    @SerializedName("appointment_time")
    @Expose
    private String appointmentTime;
    @SerializedName("added_datetime")
    @Expose
    private String addedDatetime;
    @SerializedName("appointment_date")
    @Expose
    private String appointmentDate;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("POC_Name")
    @Expose
    private String pOCName;
    @SerializedName("Institution_Name")
    @Expose
    private String institutionName;
    @SerializedName("marketing_user_employee_id")
    @Expose
    private String marketingUserEmployeeId;
    @SerializedName("Institution_Landline_Number")
    @Expose
    private Integer institutionLandlineNumber;
    @SerializedName("POC_Designation")
    @Expose
    private String pOCDesignation;
    @SerializedName("POC_Phone_Number")
    @Expose
    private Float pOCPhoneNumber;
    @SerializedName("Address_Line_2")
    @Expose
    private String addressLine2;
    @SerializedName("marketing_user_lastname")
    @Expose
    private String marketingUserLastname;
    @SerializedName("image_address")
    @Expose
    private String imageAddress;
    @SerializedName("updated_datetime")
    @Expose
    private String updatedDatetime;
    @SerializedName("POC_Email")
    @Expose
    private String pOCEmail;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("appointment_status")
    @Expose
    private String appointmentStatus;
    @SerializedName("marketing_user_picture")
    @Expose
    private String marketingUserPicture;
    @SerializedName("Institution_Email")
    @Expose
    private String institutionEmail;
    @SerializedName("marketing_user_firstname")
    @Expose
    private String marketingUserFirstname;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("Image_Selection_View")
    @Expose
    private String imageSelectionView;
    @SerializedName("Institution_STD_Code")
    @Expose
    private Integer institutionSTDCode;
    @SerializedName("Address_Line_1")
    @Expose
    private String addressLine1;
    @SerializedName("Land_Mark")
    @Expose
    private String landMark;
    @SerializedName("marketing_user_id")
    @Expose
    private String marketingUserId;
    @SerializedName("Institution_Extention_Number")
    @Expose
    private Integer institutionExtentionNumber;
    @SerializedName("marketing_user_mobile_number")
    @Expose
    private Integer marketingUserMobileNumber;

    public Unassigned() {

    }

    public Unassigned(String institutionStatus, String site, Integer pIN, String appointmentId, String customAppointmentId, String appointmentTime, String addedDatetime, String appointmentDate, String state, String pOCName, String institutionName, String marketingUserEmployeeId, Integer institutionLandlineNumber, String pOCDesignation, Float pOCPhoneNumber, String addressLine2, String marketingUserLastname, String imageAddress, String updatedDatetime, String pOCEmail, String message, String appointmentStatus, String marketingUserPicture, String institutionEmail, String marketingUserFirstname, String city, String imageSelectionView, Integer institutionSTDCode, String addressLine1, String landMark, String marketingUserId, Integer institutionExtentionNumber, Integer marketingUserMobileNumber) {
        this.institutionStatus = institutionStatus;
        this.site = site;
        this.pIN = pIN;
        this.appointmentId = appointmentId;
        this.customAppointmentId = customAppointmentId;
        this.appointmentTime = appointmentTime;
        this.addedDatetime = addedDatetime;
        this.appointmentDate = appointmentDate;
        this.state = state;
        this.pOCName = pOCName;
        this.institutionName = institutionName;
        this.marketingUserEmployeeId = marketingUserEmployeeId;
        this.institutionLandlineNumber = institutionLandlineNumber;
        this.pOCDesignation = pOCDesignation;
        this.pOCPhoneNumber = pOCPhoneNumber;
        this.addressLine2 = addressLine2;
        this.marketingUserLastname = marketingUserLastname;
        this.imageAddress = imageAddress;
        this.updatedDatetime = updatedDatetime;
        this.pOCEmail = pOCEmail;
        this.message = message;
        this.appointmentStatus = appointmentStatus;
        this.marketingUserPicture = marketingUserPicture;
        this.institutionEmail = institutionEmail;
        this.marketingUserFirstname = marketingUserFirstname;
        this.city = city;
        this.imageSelectionView = imageSelectionView;
        this.institutionSTDCode = institutionSTDCode;
        this.addressLine1 = addressLine1;
        this.landMark = landMark;
        this.marketingUserId = marketingUserId;
        this.institutionExtentionNumber = institutionExtentionNumber;
        this.marketingUserMobileNumber = marketingUserMobileNumber;
    }

    public String getInstitutionStatus() {
        return institutionStatus;
    }

    public void setInstitutionStatus(String institutionStatus) {
        this.institutionStatus = institutionStatus;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public Integer getpIN() {
        return pIN;
    }

    public void setpIN(Integer pIN) {
        this.pIN = pIN;
    }

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public String getCustomAppointmentId() {
        return customAppointmentId;
    }

    public void setCustomAppointmentId(String customAppointmentId) {
        this.customAppointmentId = customAppointmentId;
    }

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public String getAddedDatetime() {
        return addedDatetime;
    }

    public void setAddedDatetime(String addedDatetime) {
        this.addedDatetime = addedDatetime;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getpOCName() {
        return pOCName;
    }

    public void setpOCName(String pOCName) {
        this.pOCName = pOCName;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getMarketingUserEmployeeId() {
        return marketingUserEmployeeId;
    }

    public void setMarketingUserEmployeeId(String marketingUserEmployeeId) {
        this.marketingUserEmployeeId = marketingUserEmployeeId;
    }

    public Integer getInstitutionLandlineNumber() {
        return institutionLandlineNumber;
    }

    public void setInstitutionLandlineNumber(Integer institutionLandlineNumber) {
        this.institutionLandlineNumber = institutionLandlineNumber;
    }

    public String getpOCDesignation() {
        return pOCDesignation;
    }

    public void setpOCDesignation(String pOCDesignation) {
        this.pOCDesignation = pOCDesignation;
    }

    public Float getpOCPhoneNumber() {
        return pOCPhoneNumber;
    }

    public void setpOCPhoneNumber(Float pOCPhoneNumber) {
        this.pOCPhoneNumber = pOCPhoneNumber;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getMarketingUserLastname() {
        return marketingUserLastname;
    }

    public void setMarketingUserLastname(String marketingUserLastname) {
        this.marketingUserLastname = marketingUserLastname;
    }

    public String getImageAddress() {
        return imageAddress;
    }

    public void setImageAddress(String imageAddress) {
        this.imageAddress = imageAddress;
    }

    public String getUpdatedDatetime() {
        return updatedDatetime;
    }

    public void setUpdatedDatetime(String updatedDatetime) {
        this.updatedDatetime = updatedDatetime;
    }

    public String getpOCEmail() {
        return pOCEmail;
    }

    public void setpOCEmail(String pOCEmail) {
        this.pOCEmail = pOCEmail;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAppointmentStatus() {
        return appointmentStatus;
    }

    public void setAppointmentStatus(String appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }

    public String getMarketingUserPicture() {
        return marketingUserPicture;
    }

    public void setMarketingUserPicture(String marketingUserPicture) {
        this.marketingUserPicture = marketingUserPicture;
    }

    public String getInstitutionEmail() {
        return institutionEmail;
    }

    public void setInstitutionEmail(String institutionEmail) {
        this.institutionEmail = institutionEmail;
    }

    public String getMarketingUserFirstname() {
        return marketingUserFirstname;
    }

    public void setMarketingUserFirstname(String marketingUserFirstname) {
        this.marketingUserFirstname = marketingUserFirstname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getImageSelectionView() {
        return imageSelectionView;
    }

    public void setImageSelectionView(String imageSelectionView) {
        this.imageSelectionView = imageSelectionView;
    }

    public Integer getInstitutionSTDCode() {
        return institutionSTDCode;
    }

    public void setInstitutionSTDCode(Integer institutionSTDCode) {
        this.institutionSTDCode = institutionSTDCode;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getLandMark() {
        return landMark;
    }

    public void setLandMark(String landMark) {
        this.landMark = landMark;
    }

    public String getMarketingUserId() {
        return marketingUserId;
    }

    public void setMarketingUserId(String marketingUserId) {
        this.marketingUserId = marketingUserId;
    }

    public Integer getInstitutionExtentionNumber() {
        return institutionExtentionNumber;
    }

    public void setInstitutionExtentionNumber(Integer institutionExtentionNumber) {
        this.institutionExtentionNumber = institutionExtentionNumber;
    }

    public Integer getMarketingUserMobileNumber() {
        return marketingUserMobileNumber;
    }

    public void setMarketingUserMobileNumber(Integer marketingUserMobileNumber) {
        this.marketingUserMobileNumber = marketingUserMobileNumber;
    }

    @Override
    public String toString() {
        return "Unassigned{" +
                "institutionStatus='" + institutionStatus + '\'' +
                ", site='" + site + '\'' +
                ", pIN=" + pIN +
                ", appointmentId='" + appointmentId + '\'' +
                ", customAppointmentId='" + customAppointmentId + '\'' +
                ", appointmentTime='" + appointmentTime + '\'' +
                ", addedDatetime='" + addedDatetime + '\'' +
                ", appointmentDate='" + appointmentDate + '\'' +
                ", state='" + state + '\'' +
                ", pOCName='" + pOCName + '\'' +
                ", institutionName='" + institutionName + '\'' +
                ", marketingUserEmployeeId='" + marketingUserEmployeeId + '\'' +
                ", institutionLandlineNumber=" + institutionLandlineNumber +
                ", pOCDesignation='" + pOCDesignation + '\'' +
                ", pOCPhoneNumber=" + pOCPhoneNumber +
                ", addressLine2='" + addressLine2 + '\'' +
                ", marketingUserLastname='" + marketingUserLastname + '\'' +
                ", imageAddress='" + imageAddress + '\'' +
                ", updatedDatetime='" + updatedDatetime + '\'' +
                ", pOCEmail='" + pOCEmail + '\'' +
                ", message='" + message + '\'' +
                ", appointmentStatus='" + appointmentStatus + '\'' +
                ", marketingUserPicture='" + marketingUserPicture + '\'' +
                ", institutionEmail='" + institutionEmail + '\'' +
                ", marketingUserFirstname='" + marketingUserFirstname + '\'' +
                ", city='" + city + '\'' +
                ", imageSelectionView='" + imageSelectionView + '\'' +
                ", institutionSTDCode=" + institutionSTDCode +
                ", addressLine1='" + addressLine1 + '\'' +
                ", landMark='" + landMark + '\'' +
                ", marketingUserId='" + marketingUserId + '\'' +
                ", institutionExtentionNumber=" + institutionExtentionNumber +
                ", marketingUserMobileNumber=" + marketingUserMobileNumber +
                '}';
    }
}
