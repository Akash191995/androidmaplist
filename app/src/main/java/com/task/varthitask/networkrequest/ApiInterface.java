package com.task.varthitask.networkrequest;

import com.task.varthitask.model.AppointmnetListPojo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("get_monthly_appointments")
    Call<AppointmnetListPojo> getBookingAppointmentList(@Field("user_employeid") String user_employeid,
                                                        @Field("status") String status,
                                                        @Field("appointment_type") String appointment_type,
                                                        @Field("month") String month);
}
