package com.task.varthitask.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.task.varthitask.R;
import com.task.varthitask.helper.PrefManager;

public class GoogleMapActivity extends FragmentActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    PrefManager prefManager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_map);

        prefManager=new PrefManager(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        LatLng TutorialsPoint = new LatLng(12.895166, 77.625855);
        mMap.addMarker(new
                MarkerOptions().position(TutorialsPoint).title(prefManager.getMarkername()));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(TutorialsPoint));
    }
}
