package com.task.varthitask.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.task.varthitask.R;
import com.task.varthitask.adapter.MonthlyAppointmentListAdapter;
import com.task.varthitask.helper.InternetCheck;
import com.task.varthitask.helper.PrefManager;
import com.task.varthitask.helper.ProgressDialog;
import com.task.varthitask.model.AppointmnetListPojo;
import com.task.varthitask.model.Unassigned;
import com.task.varthitask.networkrequest.ApiClient;
import com.task.varthitask.networkrequest.ApiInterface;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    MonthlyAppointmentListAdapter monthlyAppointmentListAdapter;
    private Activity activity;
    private Context context;
    private RecyclerView.LayoutManager layoutManager;
    RecyclerView rvAppointmnetlist;
    private ApiInterface apiService;
    List<Unassigned> unassignedsList=new ArrayList<>();
    Toolbar toolbar;
    PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity=MainActivity.this;
        context=MainActivity.this;
        apiService = ApiClient.getClient().create(ApiInterface.class);
        prefManager=new PrefManager(context);

        rvAppointmnetlist=findViewById(R.id.rv_appointment_list);
        toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Test App");

        getBookingAppointmentList();

        layoutManager = new LinearLayoutManager(activity);
        rvAppointmnetlist.setLayoutManager(layoutManager);
        monthlyAppointmentListAdapter = new MonthlyAppointmentListAdapter(context, unassignedsList, new MonthlyAppointmentListAdapter.MonthlyAppointmentInterface() {
            @Override
            public void onClickMapIcon(String locationName, String pincode) {
                prefManager.markername(locationName);
                startActivity(new Intent(context, GoogleMapActivity.class));
            }
        });
        rvAppointmnetlist.setAdapter(monthlyAppointmentListAdapter);
    }

    public void getBookingAppointmentList() {
        new InternetCheck(new InternetCheck.Consumer() {
            @Override
            public void accept(Boolean internet) {
                if (internet) {
                    ProgressDialog.showProgressDialog(context, "Please wait...");
                    Call<AppointmnetListPojo> call = apiService.getBookingAppointmentList(
                            "VI020PE0016","All","Employee","2020-07-25");
                    call.enqueue(new Callback<AppointmnetListPojo>() {
                        @Override
                        public void onResponse(Call<AppointmnetListPojo> call, final Response<AppointmnetListPojo> response) {
                            switch (response.code()) {
                                case 200:
                                    unassignedsList.addAll(response.body().getUnassigned());
                                    monthlyAppointmentListAdapter.notifyDataSetChanged();
                                    ProgressDialog.hideProgressDialog();
                                    break;

                                case 404:
                                    ProgressDialog.hideProgressDialog();
                                    break;


                                default:
                                    ProgressDialog.hideProgressDialog();
                                    break;
                            }
                        }

                        @Override
                        public void onFailure(Call<AppointmnetListPojo> call, Throwable t) {
                            Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                            ProgressDialog.hideProgressDialog();
                            Log.d("TAG", "onFailure: AppointmnetListPojo : " + t.getMessage());
                        }
                    });
                } else {
                    Toast.makeText(context, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
